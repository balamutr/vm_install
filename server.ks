#version=RHEL7
install
text
lang en_US.UTF-8
keyboard ru
logging --level=debug
timezone Europe/Berlin
auth --useshadow --passalgo=sha256
firewall --disabled
services --enabled=sshd
eula --agreed

##configure network
network --onboot=on --activate --noipv6 --device=ens18 --bootproto=static --ip=192.168.12.100 --netmask=255.255.255.0 --gateway=192.168.12.1 --nameserver=8.8.8.8 --device=ens18

%pre
#!/bin/bash
DISK=$(lsblk --output NAME,TYPE | grep disk | head -n1 | cut -d" " -f1)
cat > /tmp/setup << EOF
ignoredisk --only-use=$DISK
clearpart --drives=$DISK --all --initlabel
bootloader --boot-drive=$DISK
zerombr
part swap --asprimary --fstype="swap" --ondisk=$DISK --recommended
part /boot --fstype ext4 --ondisk=$DISK --recommended
part / --fstype ext4 --ondisk=$DISK --size 8192 --grow
EOF
echo < /tmp/setup

NET_CFG=$(grep -oE 'net_cfg=#.*#' < /proc/cmdline)
HOST_NAME=${HOST_NAME##*=}
if [ ! -z "$NET_CFG" ]; then
  NET_CFG=${NET_CFG#*=}
  echo "${NET_CFG//#/}" >> /tmp/setup
fi
echo < /tmp/setup

ACTION=$(grep -oE 'action=[a-z]+' < /proc/cmdline)
if [ -z "$ACTION" ]; then
  echo "poweroff" >> /tmp/setup
else
  case "${ACTION##*=}" in
    "reboot")
      echo "${ACTION##*=}" >> /tmp/setup
    ;;
    *)
      echo "poweroff" >> /tmp/setup
    ;;
  esac
fi
echo < /tmp/setup
%end

%include /tmp/setup

rootpw --plaintext centos

## repos
repo --name=updates --baseurl=http://ftp.hosteurope.de/mirror/centos.org/7/updates/x86_64/
repo --name=extras --baseurl=http://ftp.hosteurope.de/mirror/centos.org/7/extras/x86_64/
repo --name=epel --baseurl=https://dl.fedoraproject.org/pub/epel/7/x86_64/
repo --name=percona --baseurl=https://repo.percona.com/yum/percona-release-latest.noarch.rpm

## network install mirror
url --url="http://ftp.hosteurope.de/mirror/centos.org/7/os/x86_64/"

%packages --ignoremissing --excludedocs
@core --nodefaults
bash-completion
epel-release
deltarpm
-NetworkManager*
-aic94xx-firmware*
-alsa-*
-iwl*firmware
-ql*firmware
-ivtv*
-plymouth*
-kexec-tools
-dracut-network
-btrfs-progs*
-postfix
-nginx
%end

%post
#!/bin/bash

##
## Ansible: Add SSH Key (Optional)
##

mkdir -m 700 -p /root/.ssh
install -b -m 600 /dev/null /root/.ssh/authorized_keys
cat > /root/.ssh/authorized_keys << EOF
ssh-rsa ...
EOF
yum -y install python

##
## END: Ansible
##

##
## Cleanup
##

yum clean all
fstrim /

##
## END: Cleanup
##

%end